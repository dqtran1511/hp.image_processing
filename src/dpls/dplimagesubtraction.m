function [dto] = dplimagesubtraction(dto)
    frames = dto.frames;
    noiseParam = dto.noiseParam;

    [height, width, ~, numFrames] = size(frames);
    greyFrames = zeros(height, width, numFrames);
    framesSubtract = zeros(height, width, numFrames, 'logical');

    for i = 1:numFrames
        greyFrames(:, :, i) = im2single(dplimagepreprocessing(frames(:, :, :, i)));

        try
            fs = abs(greyFrames(:, :, i) - greyFrames(:, :, i - 1));
        catch
            fs = uint8(framesSubtract(:, :, i));
        end

        framesSubtract(:, :, i) = dplimagebinarisation(fs);
        framesSubtract(:, :, i) = dplnoisereduction(framesSubtract(:, :, i), noiseParam);
        [row, col] = find(framesSubtract(:, :, i) == 1);

        if max(row) - min(row) > height * 0.8 & max(col) - min(col) > width * 0.8
            framesSubtract(:, :, i) = 0;
        end

    end

    dto.framesSubtract = framesSubtract;
end
