function [dto] = dplinit(dto)
    rawFrames = dto.rawFrames;
    frames = imresize(rawFrames, 0.5, 'nearest');
    dto.frames = frames;
end
