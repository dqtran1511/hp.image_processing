function [dto] = dplpeopledetection(dto)
    humanFrames = dto.humanFrames;
    humanImages = zeros(size(humanFrames));
    numFrames = length(humanFrames);
    peopleDetector = vision.PeopleDetector;

    for i = 1:numFrames
        [box, score] = peopleDetector(humanFrames(:, :, :, i));

        if sum(sum(box)) ~= 0
            humanImages(:, :, :, i) = insertObjectAnnotation( ...
                humanFrames(:, :, :, i), 'rectangle', box, score);
        end

    end

    humanImages(humanImages == 0) = [];

    dto.humanImages = humanImages;

end
