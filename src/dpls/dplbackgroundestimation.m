function [dto] = dplbackgroundestimation(dto)
    frames = dto.frames;

    R = squeeze(frames(:, :, 1, :));
    G = squeeze(frames(:, :, 2, :));
    B = squeeze(frames(:, :, 3, :));

    backgroundR = uint8(mode(R, 3));
    backgroundG = uint8(mode(G, 3));
    backgroundB = uint8(mode(B, 3));
    backgroundImage = cat(3, backgroundR, backgroundG, backgroundB);

    dto.backgroundImage = backgroundImage;

end
