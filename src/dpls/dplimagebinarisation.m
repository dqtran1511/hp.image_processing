function [binaryImage] = dplimagebinarisation(image)
    binaryImage = imbinarize(image, graythresh(image));
end
