function [dto] = dplshapedeformation(dto)
    widthMotion = dto.widthMotion;
    heightMotion = dto.heightMotion;
    numFrames = length(widthMotion);

    shapeDeformationRatio = zeros(numFrames, 1);

    for i = 1:numFrames

        if isnan(widthMotion(i)) && isnan(heightMotion(i))
            shapeDeformationRatio(i) = NaN;
        else
            shapeDeformationRatio(i) = widthMotion(i) / heightMotion(i);
        end

    end

    dto.shapeDeformationRatio = shapeDeformationRatio;
    dto.output = shapeDeformationRatio;
end
