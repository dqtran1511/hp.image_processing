function [dto] = dplobjectlocating(dto)
    frames = dto.frames;
    framesSubtract = dto.framesSubtract;
    sizeFramesSubtract = size(framesSubtract);
    framesMotion = zeros(sizeFramesSubtract);
    numFrames = sizeFramesSubtract(end);

    motionFrames = zeros(size(frames), 'uint8');
    nonMotionFrames = zeros(size(frames), 'uint8');
    widthMotion = zeros(numFrames);
    heightMotion = zeros(numFrames);
    maxRow = zeros(numFrames);
    maxCol = zeros(numFrames);
    minRow = zeros(numFrames);
    minCol = zeros(numFrames);

    iMotionFrames = 0;
    iNonMotionFrames = 0;
    objLocate = zeros(size(frames), 'uint8');

    for i = 1:numFrames

        try
            framesMotion(:, :, i) = framesSubtract(:, :, i) | ...
                framesSubtract(:, :, i - 1) | ...
                framesSubtract(:, :, i - 2);
        catch

            try
                framesMotion(:, :, i) = framesSubtract(:, :, i) | ...
                    framesSubtract(:, :, i - 1);
            catch
                framesMotion(:, :, i) = framesSubtract(:, :, i);
            end

        end

        [row, col] = find(framesMotion(:, :, i) == 1);
        mask = false(size(framesMotion(:, :, i)));

        if framesMotion(:, :, i) == 0
            iNonMotionFrames = iNonMotionFrames + 1;
            maskValue = 0;
            nonMotionFrames(:, :, :, iNonMotionFrames) = frames(:, :, :, i);
            widthMotion(i) = NaN;
            heightMotion(i) = NaN;
        else
            iMotionFrames = iMotionFrames + 1;
            maskValue = 1;
            motionFrames(:, :, :, iMotionFrames) = frames(:, :, :, i);
            widthMotion(i) = max(col) - min(col);
            heightMotion(i) = max(row) - min(row);
            maxRow(iMotionFrames) = max(row);
            maxCol(iMotionFrames) = max(col);
            minRow(iMotionFrames) = min(row);
            minCol(iMotionFrames) = min(col);
        end

        mask(min(row):max(row), min(col):max(col)) = maskValue;
        mask = bwperim(mask, 8);
        mask = imdilate(mask, strel('square', 3));

        R = frames(:, :, 1, i);
        G = frames(:, :, 2, i);
        B = frames(:, :, 3, i);

        R(mask) = 255;
        G(mask) = 0;
        B(mask) = 0;

        objLocate(:, :, :, i) = cat(3, R, G, B);

    end

    motionFrames(:, :, :, iMotionFrames + 1:end) = [];
    nonMotionFrames(:, :, :, iNonMotionFrames + 1:end) = [];
    maxRow(iMotionFrames + 1:end) = [];
    maxCol(iMotionFrames + 1:end) = [];
    minRow(iMotionFrames + 1:end) = [];
    minCol(iMotionFrames + 1:end) = [];

    dto.objLocate = objLocate;
    dto.output = objLocate;
    dto.motionFrames = motionFrames;
    dto.nonMotionFrames = nonMotionFrames;
    dto.widthMotion = widthMotion;
    dto.heightMotion = heightMotion;
    dto.maxRow = maxRow;
    dto.minRow = minRow;
    dto.maxCol = maxCol;
    dto.minCol = minCol;

end
