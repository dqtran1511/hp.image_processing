function [denoiseImage] = dplnoisereduction(image, noiseParam)
    denoiseImage = bwareaopen(imfill(image, 'holes'), noiseParam);
end
