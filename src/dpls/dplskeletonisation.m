function [dto] = dplskeletonisation(dto)
    foregroundMask = dto.foregroundMask;
    numFrames = size(foregroundMask, 3);
    skeletonImages = zeros(size(foregroundMask));

    for i = 1:numFrames
        skeletonImages(:, :, i) = bwskel(foregroundMask(:, :, i));
    end

    dto.skeletonImages = skeletonImages;
end
