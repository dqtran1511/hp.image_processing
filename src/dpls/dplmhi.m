function [dto] = dplmhi(dto)
    framesSubtract = dto.framesSubtract;
    [height, width, numFrames] = size(framesSubtract);
    mhi = zeros(size(framesSubtract), 'uint8');
    maxValue = 255;
    rate = 10;

    for i = 2:numFrames

        for h = 1:height

            for w = 1:width

                if framesSubtract(h, w, i) == true
                    mhi(h, w, i) = maxValue;
                elseif mhi(h, w, i - 1) ~= 0
                    mhi(h, w, i) = mhi(h, w, i - 1) - maxValue / rate;
                else
                    mhi(h, w, i) = 0;
                end

            end

        end

    end

    dto.mhi = mhi;
end
