function [outPredict, outReal, predictor] = dplpredictor(data, predictStep, numStep)

    step = predictStep - 1;

    if strcmp(numStep, 'all')
        numStep = step;
    end

    signal = data(step - numStep + 1:step);
    [correlation, lag] = xcorr(signal, 'biased');
    correlation(lag < 0) = [];
    [h, ~] = levinson(correlation, numel(correlation) - 1);
    predictor =- (flip(h))';
    predictor = predictor(1:end - 1);
    outPredict = 1 + signal(end - length(predictor) + 1:end) * predictor;

    try
        outReal = data(predictStep);
    catch
        outReal = NaN;
    end

end
