function grayImage = dplimagepreprocessing(rgbImage)
    grayImage = rgb2gray(rgbImage);
end
