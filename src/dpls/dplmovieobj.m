function [dto] = dplmovieobj(dto)
    output = dto.output;
    sizeOut = size(output);
    movieObj = struct('cdata', zeros(sizeOut(1:end - 1), 'uint8'), 'colormap', []);

    for i = 1:sizeOut(end)

        if numel(sizeOut) == 4
            movieObj(i).cdata = output(:, :, :, i);
        else
            movieObj(i).cdata = output(:, :, i);
        end

    end

    dto.movieObj = movieObj;
end
