function [dto] = dpl2dfpe(dto)
    skeletonImages = dto.skeletonImages;
    maxRow = dto.maxRow;
    maxCol = dto.maxCol;
    minRow = dto.minRow;
    minCol = dto.minCol;
    [height, width, numFrames] = size(skeletonImages);

    pointRGBImages = zeros(height, width, 3, numFrames, 'uint8');
    blankBackground = 255 * ones(height, width, 3, 'uint8');

    feet = zeros(2, 2);
    head = zeros(2);
    body = zeros(2);

    feet1X = zeros(numFrames, 1);
    feet1Y = zeros(numFrames, 1);
    feet2X = zeros(numFrames, 1);
    feet2Y = zeros(numFrames, 1);
    headX = zeros(numFrames, 1);
    headY = zeros(numFrames, 1);
    bodyX = zeros(numFrames, 1);
    bodyY = zeros(numFrames, 1);

    for i = 1:numFrames
        flagFeet = 0;
        flagHead = 0;
        flagBody = 0;

        for h = maxRow(i):-1:minRow(i)

            for w = minCol(i):maxCol(i)

                if skeletonImages(h, w, i) == 1
                    flagFeet = flagFeet + 1;
                    feet(flagFeet, 1) = h;
                    feet(flagFeet, 2) = w;
                end

                if flagFeet == 2
                    tol = 20;
                    intH = feet(1, 1) - tol:feet(1, 1) + tol;
                    intW = feet(1, 2) - tol:feet(1, 2) + tol;

                    if ismember(feet(2, 1), intH) && ismember(feet(2, 2), intW)
                        flagFeet = 1;
                    else
                        break
                    end

                end

            end

        end

        for h = minRow(i):maxRow(i)

            for w = minCol(i):maxCol(i)

                if skeletonImages(h, w, i) == 1
                    flagHead = flagHead + 1;
                    head(1) = h;
                    head(2) = w;
                end

                if flagHead == 1
                    break
                end

            end

        end

        for h = minRow(i):maxRow(i)

            for w = minCol(i):maxCol(i)

                if skeletonImages(h, w, i) == 1 && h == round((feet(1, 1) + head(1, 1)) / 2)
                    flagBody = flagBody + 1;
                    body(1) = h;
                    body(2) = w;
                end

                if flagBody == 1
                    break
                end

            end

        end

        markerPosition = [feet(1, 2) feet(1, 1); feet(2, 2) feet(2, 1); ...
                        body(2) body(1); head(2) head(1)];
        markerColour = {'red', 'blue', 'black', 'magenta'};
        pointRGBImages(:, :, :, i) = insertMarker(blankBackground ...
            , markerPosition, 'Color', markerColour, 'size', 5);
        feet1X(i) = feet(1, 2);
        feet1Y(i) = height - feet(1, 1);
        feet2X(i) = feet(2, 2);
        feet2Y(i) = height - feet(2, 1);

        headX(i) = head(2);
        headY(i) = height - head(1);
        bodyX(i) = body(2);
        bodyY(i) = height - body(1);

    end

    dto.feet1X = feet1X;
    dto.feet1Y = feet1Y;
    dto.feet2X = feet2X;
    dto.feet2Y = feet2Y;
    dto.headX = headX;
    dto.headY = headY;
    dto.bodyX = bodyX;
    dto.bodyY = bodyY;
    dto.pointRGBImages = pointRGBImages;

end
