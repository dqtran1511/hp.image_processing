function [dto] = dplpixelcomparison(dto)

    backgroundImage = dto.backgroundImage;
    nonMotionFrames = dto.nonMotionFrames;
    motionFrames = dto.motionFrames;
    noiseParam = dto.noiseParam;
    tolerance = dto.tolerance;

    [height, width, layers] = size(backgroundImage);

    nNonMotionFrames = size(nonMotionFrames, 4);
    nMotionFrames = size(motionFrames, 4);
    backgroundReference = zeros(height, width, layers, nNonMotionFrames, 'uint8');
    greyBackground = dplimagepreprocessing(backgroundImage);

    for i = 1:nNonMotionFrames
        backgroundReference(:, :, :, i) = nonMotionFrames(:, :, :, i);
    end

    foregroundMask = zeros(height, width, nMotionFrames, 'logical');

    for i = 1:nMotionFrames
        greyFrames = dplimagepreprocessing(motionFrames(:, :, :, i));
        foregroundMask(:, :, i) = greyFrames < greyBackground * (1 - tolerance / 100) | ...
            greyFrames > greyBackground * (1 + tolerance / 100);
        foregroundMask(:, :, i) = dplnoisereduction(foregroundMask(:, :, i), noiseParam);
    end

    dto.foregroundMask = foregroundMask;
    dto.output = foregroundMask;

end
