function [dto] = dplevalnonmotion(dto)
    nonMotionFrames = dto.nonMotionFrames;
    backgroundImage = dto.backgroundImage;
    tolerance = dto.tolerance;
    noiseParam = dto.noiseParam;

    humanFrames = zeros(size(nonMotionFrames), 'uint8');

    numFrames = size(nonMotionFrames, 4);
    greyBackground = dplimagepreprocessing(backgroundImage);

    indexHuman = 0;

    for i = 1:numFrames
        greyNonMotionFrame = dplimagepreprocessing(nonMotionFrames(:, :, :, i));
        foregroundMask = greyNonMotionFrame < greyBackground * (1 - tolerance / 100) ...
            | greyNonMotionFrame > greyBackground * (1 + tolerance / 100);
        foregroundMask = dplnoisereduction(foregroundMask, noiseParam);

        if foregroundMask == 1
            indexHuman = indexHuman + 1;
            humanFrames(:, :, :, indexHuman) = nonMotionFrames(:, :, :, i);
        end

    end

    humanFrames(indexHuman + 1:end) = [];

    dto.humanFrames = humanFrames;

end
