classdef dtoshapedeformation

    properties
        widthMotion
        heightMotion

        shapeDeformationRatio
        output
    end

end
