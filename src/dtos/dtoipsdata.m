classdef dtoipsdata

    properties
        objLocate
        foregroundMask
        mhi
        shapeDeformationRatio
        skeletonImages
        feet1X
        feet1Y
        feet2X
        feet2Y
        headX
        headY
        bodyX
        bodyY
        pointRGBImages

        motionFrames
        motionDuration

        nonMotionFrames
        nonMotionDuration

        humanImagesNonMotion
        nonMotionHumanDuration

        ratioFeet1
        ratioFeet2
        ratioHead
        ratioBody

        ratioFeetHead
        ratioFeetBody
    end

end
