classdef dtomain

    properties
        fileName
        noiseParam
        tolerance

        duration
        frameRate
        rawFrames
        frames
    end

end
