classdef dto2dfpe

    properties
        foregroundMask
        skeletonImages

        maxCol
        minCol
        maxRow
        minRow
        feet1X
        feet1Y
        feet2X
        feet2Y
        headX
        headY
        bodyX
        bodyY
        pointRGBImages

    end

end
