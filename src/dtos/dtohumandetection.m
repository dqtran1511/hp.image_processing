classdef dtohumandetection

    properties
        backgroundImage
        nonMotionFrames
        noiseParam
        tolerance

        humanFrames
        humanImages
    end

end
