classdef dtofeatureextraction

    properties
        mhi % MHI
        shapeDeformationRatio

        feet1X
        feet1Y
        feet2X
        feet2Y
        headX
        headY
        bodyX
        bodyY
        skeletonImages
        pointRGBImages

        humanImages
    end

end
