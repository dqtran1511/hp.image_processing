classdef dtoforegrounddetection

    properties
        frames
        noiseParam
        tolerance

        backgroundImage
        motionFrames
        nonMotionFrames
        backgroundReference

        foregroundMask
        output
        movieObj
    end

end
