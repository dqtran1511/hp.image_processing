classdef dtomotiondetection

    properties
        frames
        noiseParam
        framesSubtract
        framesMotion

        motionFrames
        nonMotionFrames
        widthMotion
        heightMotion
        maxRow
        maxCol
        minRow
        minCol

        objLocate
        output
        movieObj
    end

end
