% IMAGE PROCESSING SYSTEM (IPS) FOR HUMAN MOTION DETECTION, LOCATION
% PREDICTION AND NORMALITY CLASSIFICATION
%
% Honours Project - 29 May 2022
%
% Author: (Dang) Quang Tran
% Electronics and Communications - Bachelor of Engineering with Honours
% Institution: University of Tasmania
%
% <help ipsgetdata> for more information about main IPS function
% <help ipsversion> for information about the last update
