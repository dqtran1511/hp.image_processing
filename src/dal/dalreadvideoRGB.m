function [dto] = dalreadvideoRGB(dto)
    fileName = dto.fileName;
    video = VideoReader(fileName);
    numFrames = video.NumFrames;
    duration = video.Duration;
    frameRate = video.FrameRate;
    frames = zeros(video.Height, video.Width, 3, numFrames, 'uint8');

    for i = 1:numFrames
        frames(:, :, :, i) = read(video, i);
    end

    dto.duration = duration;
    dto.frameRate = frameRate;
    dto.rawFrames = frames;
end
