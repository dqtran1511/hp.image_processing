function [dtoFeatureExtraction] = modulefeatureextraction(dtoMotionDetection, dtoForegroundDetection)

    dtoMHI = dtomhi;
    dtoShapeDeformation = dtoshapedeformation;
    dto2DFPE = dto2dfpe;
    dtoHumanDetection = dtohumandetection;
    dtoFeatureExtraction = dtofeatureextraction;

    dtoMHI.framesSubtract = dtoMotionDetection.framesSubtract;
    dtoMHI = dplmhi(dtoMHI);

    dtoShapeDeformation.widthMotion = dtoMotionDetection.widthMotion;
    dtoShapeDeformation.heightMotion = dtoMotionDetection.heightMotion;
    dtoShapeDeformation = dplshapedeformation(dtoShapeDeformation);

    dto2DFPE.foregroundMask = dtoForegroundDetection.foregroundMask;
    dto2DFPE.maxCol = dtoMotionDetection.maxCol;
    dto2DFPE.maxRow = dtoMotionDetection.maxRow;
    dto2DFPE.minCol = dtoMotionDetection.minCol;
    dto2DFPE.minRow = dtoMotionDetection.minRow;
    dto2DFPE = dplskeletonisation(dto2DFPE);
    dto2DFPE = dpl2dfpe(dto2DFPE);

    dtoHumanDetection.nonMotionFrames = dtoMotionDetection.nonMotionFrames;
    dtoHumanDetection.backgroundImage = dtoForegroundDetection.backgroundImage;
    dtoHumanDetection.noiseParam = dtoForegroundDetection.noiseParam;
    dtoHumanDetection.tolerance = dtoForegroundDetection.tolerance;
    dtoHumanDetection = dplevalnonmotion(dtoHumanDetection);
    dtoHumanDetection = dplpeopledetection(dtoHumanDetection);

    dtoFeatureExtraction.mhi = dtoMHI.mhi;
    dtoFeatureExtraction.shapeDeformationRatio = ...
        dtoShapeDeformation.shapeDeformationRatio;
    dtoFeatureExtraction.feet1X = dto2DFPE.feet1X;
    dtoFeatureExtraction.feet1Y = dto2DFPE.feet1Y;
    dtoFeatureExtraction.feet2X = dto2DFPE.feet2X;
    dtoFeatureExtraction.feet2Y = dto2DFPE.feet2Y;
    dtoFeatureExtraction.headX = dto2DFPE.headX;
    dtoFeatureExtraction.headY = dto2DFPE.headY;
    dtoFeatureExtraction.bodyX = dto2DFPE.bodyX;
    dtoFeatureExtraction.bodyY = dto2DFPE.bodyY;
    dtoFeatureExtraction.humanImages = dtoHumanDetection.humanImages;
    dtoFeatureExtraction.skeletonImages = dto2DFPE.skeletonImages;
    dtoFeatureExtraction.pointRGBImages = dto2DFPE.pointRGBImages;

end
