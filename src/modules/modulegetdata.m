function dtoData = modulegetdata(dtoMain)
    dtoMain = moduleinit(dtoMain);

    dtoMotionDetection = dtomotiondetection;
    dtoMotionDetection.frames = dtoMain.frames;
    dtoMotionDetection.noiseParam = dtoMain.noiseParam;
    dtoMotionDetection = modulemotiondetection(dtoMotionDetection);

    dtoForegroundDetection = dtoforegrounddetection;
    dtoForegroundDetection.frames = dtoMain.frames;
    dtoForegroundDetection.noiseParam = dtoMain.noiseParam;
    dtoForegroundDetection.tolerance = dtoMain.tolerance;
    dtoForegroundDetection.motionFrames = dtoMotionDetection.motionFrames;
    dtoForegroundDetection.nonMotionFrames = dtoMotionDetection.nonMotionFrames;
    dtoForegroundDetection = moduleforegrounddetection(dtoForegroundDetection);

    dtoFeatureExtraction = modulefeatureextraction( ...
        dtoMotionDetection, dtoForegroundDetection);

    dtoData = moduledataprocessing(dtoMain, dtoMotionDetection ...
        , dtoForegroundDetection, dtoFeatureExtraction);
end
