function [dto] = moduleforegrounddetection(dto)
    dto = dplbackgroundestimation(dto);
    dto = dplpixelcomparison(dto);
    dto = dplmovieobj(dto);
end
