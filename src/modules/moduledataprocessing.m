function [dtoData] = moduledataprocessing(dtoMain, dtoMDS, dtoFDS, dtoFES)
    dtoData = dtoipsdata;
    dtoData.objLocate = dtoMDS.objLocate;
    dtoData.foregroundMask = dtoFDS.foregroundMask;
    dtoData.mhi = dtoFES.mhi;
    dtoData.shapeDeformationRatio = dtoFES.shapeDeformationRatio;
    dtoData.feet1X = dtoFES.feet1X;
    dtoData.feet1Y = dtoFES.feet1Y;
    dtoData.feet2X = dtoFES.feet2X;
    dtoData.feet2Y = dtoFES.feet2Y;
    dtoData.headX = dtoFES.headX;
    dtoData.headY = dtoFES.headY;
    dtoData.bodyX = dtoFES.bodyX;
    dtoData.bodyY = dtoFES.bodyY;
    dtoData.skeletonImages = dtoFES.skeletonImages;
    dtoData.pointRGBImages = dtoFES.pointRGBImages;
    dtoData.motionFrames = dtoMDS.motionFrames;
    dtoData.nonMotionFrames = dtoMDS.nonMotionFrames;
    dtoData.humanImagesNonMotion = dtoFES.humanImages;

    nMotionFrames = size(dtoMDS.motionFrames, 4);
    dtoData.motionDuration = nMotionFrames / dtoMain.frameRate;

    nNonMotionFrames = size(dtoMDS.nonMotionFrames, 4);
    dtoData.nonMotionDuration = nNonMotionFrames / dtoMain.frameRate;

    nHumanImages = size(dtoFES.humanImages, 4);
    dtoData.nonMotionHumanDuration = nHumanImages / dtoMain.frameRate;

    ratioFeet1 = zeros(nMotionFrames, 1);
    ratioFeet2 = zeros(nMotionFrames, 1);
    ratioHead = zeros(nMotionFrames, 1);
    ratioBody = zeros(nMotionFrames, 1);
    ratioFeetHead = zeros(nMotionFrames, 1);
    ratioFeetBody = zeros(nMotionFrames, 1);

    for i = 1:nMotionFrames
        ratioFeet1(i) = (dtoMDS.maxCol(i) - dtoData.feet1X(i)) ...
            / (dtoData.feet1X(i) - dtoMDS.minCol(i));
        ratioFeet2(i) = (dtoMDS.maxCol(i) - dtoData.feet2X(i)) ...
            / (dtoData.feet2X(i) - dtoMDS.minCol(i));
        ratioHead(i) = (dtoMDS.maxCol(i) - dtoData.headX(i)) ...
            / (dtoData.headX(i) - dtoMDS.minCol(i));
        ratioBody(i) = (dtoMDS.maxCol(i) - dtoData.bodyX(i)) ...
            / (dtoData.bodyX(i) - dtoMDS.minCol(i));

        ratioFeetHead(i) = (dtoData.feet1X(i) - dtoData.headX(i)) ...
            / (dtoData.headX(i) - dtoData.feet2X(i));
        ratioFeetBody(i) = (dtoData.feet1X(i) - dtoData.bodyX(i)) ...
            / (dtoData.bodyX(i) - dtoData.feet2X(i));
    end

    dtoData.ratioFeet1 = ratioFeet1;
    dtoData.ratioFeet2 = ratioFeet2;
    dtoData.ratioHead = ratioHead;
    dtoData.ratioBody = ratioBody;
    dtoData.ratioFeetHead = ratioFeetHead;
    dtoData.ratioFeetBody = ratioFeetBody;
end
