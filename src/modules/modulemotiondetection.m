function [dto] = modulemotiondetection(dto)
    dto = dplimagesubtraction(dto);
    dto = dplobjectlocating(dto);
    dto = dplmovieobj(dto);
end
