% Last update (System level): 2 June 2022
% Built: MATLAB R2022a
%
% UPDATE:
%   - Optimise algorithms for pixel computing
%   - Finalise IPS structure
%   - Rename files, functions and variables for style convention
%   - Add documentation
