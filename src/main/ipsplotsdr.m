function [] = ipsplotsdr(dtoData)
    figure('color', 'white')
    plot(1:length(dtoData.shapeDeformationRatio), dtoData.shapeDeformationRatio);
    hold on;
    yline(1, '--black');
    yline(0.5, '-.', 'color', [.5 .5 .5]);
    hold on
    xlabel('Frame index'); ylabel('Shape Deformation Ratio');
    legend('SDR', 'ratio = 1', 'ratio = 0.5', 'Location', 'EastOutside');
    title('Shape Deformation Ratio data')
    grid on
end
