function [] = ipsshowimages(frames)
    nDims = ndims(frames);
    nFrames = size(frames, nDims);

    for i = 1:nFrames

        if nDims == 3
            imshow(frames(:, :, i));
        else
            imshow(frames(:, :, :, i));
        end

    end

end
