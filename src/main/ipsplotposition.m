function [] = ipsplotposition(dtoData)
    figure('Color', 'white')
    plot(dtoData.headX, dtoData.headY, '.b');
    hold on
    plot(dtoData.bodyX, dtoData.bodyY, '.r');
    hold on
    legend('Head', 'Body', 'Location', 'EastOutside')
    xlabel('X coordinate'); ylabel('Y coordinate');
    title('Position data')
    grid on
end
