function ipsdata = ipsgetdata(fileName, nosieParam, tolerance, option1, option2)
    %IPSGETDATA     get data extracted from image processing system (IPS) for input video
    %   ipsdata = IPSGETDATA(fileName,noiseParam,tolerance)
    %       fileName: name of video in string format, must be in the same path
    %       noiseParam: parameter for noise reduction, i.e., 20.
    %       tolerance: parameter for foreground detection, i.e., 50.
    %
    %   ipsdata = IPSGETDATA(fileName,noiseParam,tolerance,option1,option2)
    %       Optional 'option1' to show images or data with optional 'option2'
    %       option1: 'images'
    %           option2: 'all', 'motionDetection', 'foregroundDetection',
    %           'mhi', 'skeletonisation', '2-dfpe', 'humanDetection'.
    %       option1: 'data'
    %           option2: 'all', 'position', 'sdr'

    dtoMain = dtomain;
    dtoMain.fileName = fileName;
    dtoMain.noiseParam = nosieParam;
    dtoMain.tolerance = tolerance;

    acceptOption1 = 'images data';
    acceptOption1 = split(acceptOption1);
    acceptOption2 = 'motionDetection foregroundDetection mhi skeletonisation 2-dfpe humanDetection sdr position';
    acceptOption2 = split(acceptOption2);

    if (exist('option1', 'var') && ~ismember(option1, acceptOption1)) || ...
            (exist('option2', 'var') && ~ismember(option2, acceptOption2))
        warning('Invalid input argument, please <help ipsgetdata> for more information');
        ipsdata = NaN;
        return
    end

    warning('off', 'MATLAB:structOnObject')
    dtoData = modulegetdata(dtoMain);
    ipsdata = struct(dtoData);

    if ~exist('option1', 'var') && ~exist('option2', 'var')
        return
    end

    switch option1
        case 'images'

            if ~exist('option2', 'var')
                option2 = 'all';
            end

            figure('color', 'white');

            switch option2
                case 'all'
                    ipsshowimages(dtoData.objLocate);
                    ipsshowimages(dtoData.foregroundMask);
                    ipsshowimages(dtoData.mhi);
                    ipsshowimages(dtoData.skeletonImages);
                    ipsshowimages(dtoData.pointRGBImages);
                    ipsshowimages(dtoData.humanImagesNonMotion);
                case 'motionDetection'
                    ipsshowimages(dtoData.objLocate);
                case 'foregroundDetection'
                    ipsshowimages(dtoData.foregroundMask);
                case 'mhi'
                    ipsshowimages(dtoData.mhi);
                case 'skeletonisation'
                    ipsshowimages(dtoData.skeletonImages);
                case '2-dfpe'
                    ipsshowimages(dtoData.skeletonImages);
                case 'humanDetection'
                    ipsshowimages(dtoData.humanImagesNonMotion);
            end

            return
        case 'data'

            if ~exist('option2', 'var')
                option2 = 'all';
            end

            switch option2
                case 'all'
                    ipsplotsdr(dtoData);
                    ipsplotposition(dtoData);
                case 'position'
                    ipsplotposition(dtoData);
                case 'sdr'
                    ipsplotsdr(dtoData);
            end

            return
    end

end
