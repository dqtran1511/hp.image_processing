% prepare dataset
% prepare validation set (train/test set)
% feature selection
% finding best hyper parameter
% test trained model with test set
% + visualize hyperplane

%%
load data % data is dataset

num = 200; % number of data used to process (train+test set)

class_num = grp2idx(class);

X = spec(1:num, :); % For all data specification, change spec according to data

y = class_num(1:num, :);

rand_num = randperm(num); % generate random index according to number of data
num_train = round(num * 0.8); % 80 % data set used for training
num_test = num - num_train; % the rest for testing

X_train = X(rand_num(1:num_train), :); % get training dataset
y_train = y(rand_num(1:num_train), :); % get training output

X_test = X(rand_num(num_train + 1:num), :); % get testing dataset
y_test = y(rand_num(num_train + 1:num), :); % get testing output

%% CV parition
c = cvpartition(y_train, 'k', 5);

%% feature selection ?optional?
opts = statset('display', 'iter');
fun = @(train_data, train_labels, test_data, test_labels) ...
    sum(predict(fitcsvm(train_data, train_labels, 'KernelFunction', 'rbf'), test_data) ~= test_labels);

[fs, history] = sequentialfs(fun, X_train, y_train, 'cv', c, 'options', opts, 'nfeatures', 10);

%% best hyperplane parameter
X_train_w_best_features = X_train(:, fs);
svm_model = fitcsvm(X_train_w_best_features, y_train, 'KernelFunction', 'rbf', ...
    'OptimizeHyperparameters', 'auto', 'HyperparameterOptimizationOptions', ...
    struct('AcquisitionFunctionName', 'expected-improvement-plus', 'ShowPlots', true));
