% <help ipsgetdata> for more information about main IPS function
% <help ipsinfo>    for more information about author
% <help ipsversion> for more information about the last update
clear all; clc; close all;
addpath(genpath('src'),genpath('videos')); % Add video source/folder if needed
tic

noiseParam = 20;
tolerance = 50;
ipsdata = ipsgetdata('n1.mp4', noiseParam, tolerance);

toc
