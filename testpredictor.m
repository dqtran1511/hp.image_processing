clear all; close all; clc
addpath(genpath('src'));

tic
%% Acquire IPS data
noiseParam = 20;
tolerance = 50;
ipsdata = ipsgetdata('n1.mp4', noiseParam, tolerance);

%% Position Data
% Reliable range of position data (ignore data saturation at the start and
% end of video). It can be observed from
% ipsgetdata(fileName,noiseParam,tolerance,'data','position')
dRange = 20:80;

% Original extracted position data is in form of column (Nx1 vector), so
% that it needs to be transpose (1xN vector) for predictor
hx = (ipsdata.headX)';
hy = (ipsdata.headY)';
bx = (ipsdata.bodyX)';
by = (ipsdata.bodyY)';

% Restore unreliable data caused by computational error
hy(hy < 5) = mean(hy);
hx = hx(dRange);
hy = hy(dRange);
by(by < 5) = mean(by);
bx = bx(dRange);
by = by(dRange);

% Initialise vector of predicted data
phx = zeros(size(hx));
phy = zeros(size(hy));
pbx = zeros(size(bx));
pby = zeros(size(by));

% Initial data to satisfy ability of predictor model
phx(1:2) = hx(1:2);
phy(1:2) = hy(1:2);
pbx(1:2) = bx(1:2);
pby(1:2) = by(1:2);

% Predict data
for i = 3:length(hx)
    [phx(i), ~, ~] = dplpredictor(hx, i, 'all');
    [phy(i), ~, ~] = dplpredictor(hy, i, 'all');
    [pbx(i), ~, ~] = dplpredictor(bx, i, 'all');
    [pby(i), ~, ~] = dplpredictor(by, i, 'all');
end

[height, width, ~, ~] = size(ipsdata.pointRGBImages);
figure('color', 'white');
title('Position prediction')
plot(hx, hy, '.b');
hold on
plot(phx, phy, '.r');
hold on
plot(bx, by, 'xb');
hold on
plot(pbx, pby, 'xr');
hold on
xlim([0 width]);
ylim([0 height]);
xlabel('X coordinate'); ylabel('Y coordinate');
legend('Actual head', 'Predicted head', 'Actual body', 'Predicted body', 'Location', 'eastoutside');
title('Position prediction')
grid on

toc
